import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @(#) RankingList.java
 */

public class RankingList
{
	private List<Player> players;
	
	public RankingList(){
		players = new ArrayList<Player>();
	}

	public List<Player> getPlayer() {
		return players;
	}

	public void addPlayer(Player player) {
		this.players.add(player);
		Collections.sort(players, new Comparator<Player>() {
			@Override
			public int compare(Player arg0, Player arg1) {
				return arg1.getScore() - arg0.getScore();
			}
		});
	}
}
