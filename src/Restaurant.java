import java.util.ArrayList;
import java.util.List;

/**
 * @(#) Restaurant.java
 */

public class Restaurant
{
	private List<Employee> employees;
	
	private List<MenuItem> dishesMenu;
	
	private List<MenuItem> beveragesMenu;
	
	private List<Table> tables;
	
	private List<Order> orders;
	
	private String name;
	
	private String adress;
	
	private String city;
	
	private int budget;
	
	private int reputationPoints;
	
	public Restaurant(){
		this.budget = 10000;
		employees = new ArrayList<Employee>();
		employees.add(new Chef("Chef","Wonderful"));
		employees.add(new Barmen("Barmen", "Gifted"));
		for (int i = 0; i < 3; i++){
			employees.add(new Waiter("Waiter", Integer.toString(i)));
		}
		tables = new ArrayList<Table>();
		for (int i = 0; i < 9; i++){
			tables.add(new Table(i+1));
		}
		orders = new ArrayList<Order>();
		reputationPoints = 15;
	}
	
	public void paySuppliers()
	{
		for (MenuItem item: dishesMenu){
			if (item.quality == Quality.Low) budget -= 3;
			else budget-=10;
		}
		for (MenuItem item: beveragesMenu){
			if (item.quality == Quality.Low) budget -= 1;
			else budget-=3;
		}
	}
	
	public void computeReputation( int clientSatisaction )
	{
		reputationPoints += clientSatisaction;
		if (reputationPoints<0) reputationPoints=0;
		if (reputationPoints>100) reputationPoints=100;
	}
	
	public void payUtilites( int amount )
	{
		budget -= amount;
	}
	
	public void paySalaries()
	{
		for (Employee item : employees){
			budget -= item.getSalary();
		}
	}
	
	public void computeStatistics( )
	{
		
	}
	
	public void payTraining( int amount )
	{
		budget -= amount;
	}
	
	public void processOrder( Order order )
	{
		budget += order.getBeverage().getPrice();
		budget += order.getMainDish().getPrice();
		orders.add(order);
	}
	
	public Employee getChef(){
		for (Employee item: employees){
			if (item.getClass().equals(Chef.class)) return item;
		}
		return null;
	}
	
	public Employee getBarmen(){
		for (Employee item: employees){
			if (item.getClass().equals(Barmen.class)) return item;
		}
		return null;
	}
	
	public int getBudget(){
		return budget;
	}
	
	public void setBudget(int budget){
		this.budget = budget;
	}

	public int getReputationPoints() {
		return reputationPoints;
	}

	public void setReputationPoints(int reputationPoints) {
		this.reputationPoints = reputationPoints;
	}

	public List<MenuItem> getDishesMenu() {
		return dishesMenu;
	}

	public void setDishesMenu(List<MenuItem> dishesMenu) {
		this.dishesMenu = dishesMenu;
	}

	public List<MenuItem> getBeveragesMenu() {
		return beveragesMenu;
	}

	public void setBeveragesMenu(List<MenuItem> beveragesMenu) {
		this.beveragesMenu = beveragesMenu;
	}
	
	public List<Employee> getEmployees(){
		return employees;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}
}
