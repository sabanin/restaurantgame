import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * @(#) GameController.java
 */

public class GameController {
	private Restaurant restourant;

	private Player player;

	private int day;

	public void chooseName(String name) {
		player.setName(name);
	}

	public Player startGame() {
		restourant = new Restaurant();
		player = new Player();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name");
		String name = sc.next();
		chooseName(name);
		// making menu
		setDishesQuality(sc);
		setBeveragesQuality(sc);
		System.out.println("Enter prices (producing costs is brackets)");
		System.out.println("Low quality dishes (3 euro)");
		int lowDcost = sc.nextInt();
		System.out.println("High quality dishes (10 euro)");
		int highDcost = sc.nextInt();
		System.out.println("Low quality beverages (1 euro)");
		int lowBcost = sc.nextInt();
		System.out.println("High quality beverages (3 euro)");
		int highBcost = sc.nextInt();
		setPrice(lowDcost, highDcost, lowBcost, highBcost);
		// generate clients
		List<Client> clients = new ArrayList<Client>();
		for (int i = 0; i < 18; i++) {
			clients.add(new Client("Client", Integer.toString(i)));
		}
		for (day = 1; (day <= 30) && (restourant.getBudget() > 0); day++) {
			System.out.println("DAY " + Integer.toString(day));
			System.out.println("Budget = "
					+ Integer.toString(restourant.getBudget()));
			System.out.println("Rating = "
					+ Integer.toString(restourant.getReputationPoints()));
			int numOfClients = countNumberOfClients(restourant
					.getReputationPoints());
			List<Table> tables = restourant.getTables();
			int tableCounter = 0;
			System.out
					.println("Wonderful! Clients starts to come. Today we expect "
							+ Integer.toString(numOfClients) + " clients");
			assignTable(sc);
			if (tables.get(tableCounter).getWaiter() == null)
				numOfClients = 0;
			while (numOfClients > 0) {
				for (int i = 0; i < 2; i++) {
					Random rand = new Random();
					Client client = clients.get(rand.nextInt(18));
					System.out.println(client.getFullName()+" comes to restaurant and making order!");
					Order order = client.makeOrder(restourant.getDishesMenu(),
							restourant.getBeveragesMenu());
					restourant.processOrder(order);
					int reputationPoints = client.getSatisfaction(
							tables.get(tableCounter).getWaiter()
									.getExperience(),
							restourant.getBarmen().experience,
							restourant.getChef().experience);
					restourant.computeReputation(reputationPoints);
					System.out.println("Client satisfaction: "
							+ Integer.toString(reputationPoints));
					numOfClients--;
				}
				tableCounter++;
				if ((tableCounter >= 9)
						|| (tables.get(tableCounter).getWaiter() == null))
					numOfClients = 0;
			}

			if (day % 7 == 0) {
				System.out.println("Pay to workers!");
				restourant.paySalaries();
				System.out.println("Pay to suppliers!");
				restourant.paySuppliers();
				System.out.println("Budget = "
						+ Integer.toString(restourant.getBudget()));
				System.out
						.println("Do you want to increase level of worker? (Y/N)");
				String answer = sc.next().toLowerCase();
				while (answer.equals("y")) {
					int numberOfEmployee = 1;
					List<Employee> employees = restourant.getEmployees();
					for (Employee item : employees) {
						if (item.getExperience() != Experience.High) {
							System.out.println(Integer
									.toString(numberOfEmployee)
									+ " "
									+ item.getFullName()
									+ " current experience is "
									+ item.getExperience().toString());
						}
						numberOfEmployee++;
					}
					System.out
							.println("Choose number of employee to increase level");
					int number = sc.nextInt() - 1;
					trainEmployee(employees.get(number));
					System.out
							.println("Do you want to increase level of another worker? (Y/N)");
					answer = sc.next().toLowerCase();
				}
			}
		}
		restourant.payUtilites(4000);
		if (restourant.getBudget() < 0)
			System.out.println("You loose! Your budget is "
					+ Integer.toString(restourant.getBudget()));
		else
			System.out.println("You win! Your score is "
					+ Integer.toString(restourant.getBudget()));
		player.setScore(restourant.getBudget());
		countStatistics(clients);
		return player;
		
	}

	public void trainEmployee(Employee employee) {
		if (employee.getExperience() == Experience.High) {
			System.out.println("Level cannot be increased! It's already high!");
		} else {
			if (employee.getClass().equals(Waiter.class)) {
				employee.increaseExp();
				restourant.payTraining(800);
			} else {
				employee.increaseExp();
				restourant.payTraining(1200);
			}
			System.out.println("Level was increased! Your budget now is "
					+ Integer.toString(restourant.getBudget()));
		}
	}

	public void assignTable(Scanner sc) {
		List<Employee> employees = restourant.getEmployees();
		List<Table> tables = restourant.getTables();
		int tableCounter = 0;
		for (Employee item : employees) {
			if (item.getClass().equals(Waiter.class)) {
				System.out
						.println("How many tables would you like to assign to "
								+ item.getFullName() + " with experience "
								+ item.getExperience().toString());
				int numOfTables = sc.nextInt();
				while (!(numOfTables >= 0 && numOfTables <= 3)) {
					System.out.println("Choose number between 0 and 3");
					numOfTables = sc.nextInt();
				}
				for (int i = numOfTables; i > 0; i--) {
					tables.get(tableCounter).setWaiter((Waiter) item);
					tableCounter++;
				}
				for (int i = tableCounter; i < tables.size(); i++) {
					tables.get(i).setWaiter(null);
				}
			}
		}
	}

	public void setDishesQuality(Scanner sc) {
		System.out.println("Choose number of Hight and Low quolity dishes");
		int hightNo = sc.nextInt();
		int lowNo = sc.nextInt();
		while ((hightNo + lowNo != 5) || (hightNo < 0) || (lowNo < 0)) {
			System.out
					.println("Sum should be 5! Choose number of Hight and Low quolity dishes");
			hightNo = sc.nextInt();
			lowNo = sc.nextInt();
		}
		List<MenuItem> dishes = new ArrayList<MenuItem>();
		for (int i = 0; i < hightNo; i++) {
			MainDish dish = new MainDish();
			dish.setQuality(Quality.High);
			dishes.add(dish);
		}
		for (int i = 0; i < lowNo; i++) {
			MainDish dish = new MainDish();
			dish.setQuality(Quality.Low);
			dishes.add(dish);
		}
		restourant.setDishesMenu(dishes);
	}

	public void setBeveragesQuality(Scanner sc) {
		System.out.println("Choose number of Hight and Low quolity beverage");
		int hightNo = sc.nextInt();
		int lowNo = sc.nextInt();
		while ((hightNo + lowNo != 5) || (hightNo < 0) || (lowNo < 0)) {
			System.out
					.println("Sum should be 5! Choose number of Hight and Low quolity beverage");
			hightNo = sc.nextInt();
			lowNo = sc.nextInt();
		}
		List<MenuItem> bevereges = new ArrayList<MenuItem>();
		for (int i = 0; i < hightNo; i++) {
			MainDish beverage = new MainDish();
			beverage.setQuality(Quality.High);
			bevereges.add(beverage);
		}
		for (int i = 0; i < lowNo; i++) {
			MainDish beverage = new MainDish();
			beverage.setQuality(Quality.Low);
			bevereges.add(beverage);
		}
		restourant.setBeveragesMenu(bevereges);
	}

	public void setPrice(int lowDCost, int highDCost, int lowBCost,
			int highBCost) {
		for (MenuItem item : restourant.getDishesMenu()) {
			if (item.getQuality() == Quality.Low)
				item.setPrice(lowDCost);
			else
				item.setPrice(highDCost);
		}
		for (MenuItem item : restourant.getBeveragesMenu()) {
			if (item.getQuality() == Quality.Low)
				item.setPrice(lowBCost);
			else
				item.setPrice(highBCost);
		}
	}

	private int countNumberOfClients(int reputation) {
		if (reputation >= 30)
			return 18;
		if (reputation >= 15)
			return 10;
		return 4;
	}

	private void countStatistics(List<Client> clients) {
		for (Client client : clients) {
			client.computeStatistics();
		}
	}
}
