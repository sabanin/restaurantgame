/**
 * @(#) Employee.java
 */

public class Employee
{
	protected String name;
	
	protected String surname;
	
	protected int salary;
	
	protected Experience experience;
	
	public Employee(String name, String surname){
		this.name = name;
		this.surname = surname;
		experience = Experience.Low;
	}
	
	public void increaseExp( )
	{
		switch (experience){
		case Low: 
			experience=Experience.Medium;
			salary += 100;
			break;
		case Medium:
			experience=Experience.High;
			salary += 100;
		}
	}
	
	public int getSalary(){
		return salary;
	}
	
	public Experience getExperience(){
		return experience;
	}
	
	public String getFullName(){
		return name+" "+surname;
	}
}
