/**
 * @(#) Waiter.java
 */

public class Waiter extends Employee
{
	public Waiter(String name, String surname){
		super(name, surname);
		salary = 200;
	}
}
