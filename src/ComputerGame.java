import java.util.Scanner;


public class ComputerGame {

	public static void main(String[] args) {
		RankingList rl = new RankingList();
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose action\n 1. Start Game\n 2. Watch ranking\n 3. Exit");
		int action = sc.nextInt();
		while(action != 3){
			if (action == 1){
				GameController gc = new GameController();
				Player player = gc.startGame();
				rl.addPlayer(player);
			}
			if (action == 2){
				System.out.println("Ranking list:");
				for (Player player : rl.getPlayer()){
					System.out.println(player.getName() + " " + player.getScore());
				}
			}
			System.out.println("Choose action\n 1. Start Game\n 2. Watch ranking\n 3. Exit");
			action = sc.nextInt();
		}
		
		
	}

}
