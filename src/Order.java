/**
 * @(#) Orders.java
 */

public class Order
{
	private String orderNo;
	
	private int clientSatisfaction;
	
	private MenuItem mainDish;
	
	private MenuItem beverage;
	
	public Order(MenuItem mainDish, MenuItem beverage){
		this.setMainDish(mainDish);
		this.setBeverage(beverage);
	}

	public MenuItem getMainDish() {
		return mainDish;
	}

	public void setMainDish(MenuItem mainDish) {
		this.mainDish = mainDish;
	}

	public MenuItem getBeverage() {
		return beverage;
	}

	public void setBeverage(MenuItem beverage) {
		this.beverage = beverage;
	}
	
}
