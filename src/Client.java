import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @(#) Client.java
 */

public class Client
{
	private MenuItem dish;
	
	private MenuItem beverage;
	
	private String name;
	
	private String surname;
	
	private String telephone;
	
	private String taxCode;
	
	private List<Order> orders;
	
	public Client(String name, String surname){
		this.name = name;
		this.surname = surname;
		orders = new ArrayList<Order>();
	}
	
	public void computeStatistics( )
	{
		int beveragePrices = 0;
		int foodPrices = 0;
		for (Order order: orders){
			beveragePrices+=order.getBeverage().getPrice();
			foodPrices+=order.getMainDish().getPrice();
		}
		System.out.println(name+" "+surname+" was in restaurant "+orders.size()+" times. Spend "+Integer.toString(foodPrices)+"$ on foods and "+Integer.toString(beveragePrices)+"$ on beverages" );
	}
	
	public Order makeOrder(List<MenuItem> dishesMenu, List<MenuItem> beveragesMenu){
		Random rand = new Random(); 
		dish = dishesMenu.get(rand.nextInt(5));
		beverage = beveragesMenu.get(rand.nextInt(5));
		Order order = new Order(dish, beverage);
		orders.add(order);
		return order;
	}
	
	public int getSatisfaction(Experience waiterExp, Experience barmenExp, Experience chefExp){
		int serviceSatisfaction = 0;
		int foodSatisfaction = 0;
		int beverageSatisfaction = 0;
		//Service satisfaction
		double randomSatisfaction = Math.random();
		switch (waiterExp){
		case Low: if (randomSatisfaction<0.6) serviceSatisfaction++; else  serviceSatisfaction--; break;
		case Medium: if (randomSatisfaction<0.8) serviceSatisfaction++; else  serviceSatisfaction--; break;
		case High: if (randomSatisfaction<0.9) serviceSatisfaction++; else  serviceSatisfaction--; break;
		}
		//Beverage Satisfaction
		double satisfactionBonus = 0;
		if (beverage.getQuality()==Quality.High){
			satisfactionBonus += 0.2;
			int diff = beverage.getPrice()-3;
			satisfactionBonus -= ((diff/3)*0.1);
		} else {
			int diff = beverage.getPrice()-1;
			satisfactionBonus -= ((diff/3)*0.1);
		}
		randomSatisfaction = Math.random();
		switch (barmenExp){
		case Low: if (randomSatisfaction<satisfactionBonus+0.4) beverageSatisfaction++; else  beverageSatisfaction--; break;
		case Medium: if (randomSatisfaction<satisfactionBonus+0.6) beverageSatisfaction++; else  beverageSatisfaction--; break;
		case High: if (randomSatisfaction<satisfactionBonus+0.8) beverageSatisfaction++; else  beverageSatisfaction--; break;
		}
		//Meal satisfaction
		satisfactionBonus = 0;
		if (dish.getQuality()==Quality.High){
			satisfactionBonus += 0.2;
			int diff = dish.getPrice()-10;
			satisfactionBonus -= ((diff/3)*0.1);
		} else {
			int diff = dish.getPrice()-3;
			satisfactionBonus -= ((diff/3)*0.1);
		}
		randomSatisfaction = Math.random();
		switch (chefExp){
		case Low: if (randomSatisfaction<satisfactionBonus+0.4) foodSatisfaction++; else  foodSatisfaction--; break;
		case Medium: if (randomSatisfaction<satisfactionBonus+0.6) foodSatisfaction++; else  foodSatisfaction--; break;
		case High: if (randomSatisfaction<satisfactionBonus+0.8) foodSatisfaction++; else  foodSatisfaction--; break;
		}
		
		return serviceSatisfaction+foodSatisfaction+beverageSatisfaction;
	}
	
	public String getFullName(){
		return name+" "+surname;
	}
}
