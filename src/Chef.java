/**
 * @(#) Chef.java
 */

public class Chef extends Employee
{
	private int taxCode;
	public Chef(String name, String surname){
		super(name, surname);
		salary = 300;
	}	
}
