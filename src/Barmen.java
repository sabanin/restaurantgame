/**
 * @(#) Barmen.java
 */

public class Barmen extends Employee
{
	public Barmen(String name, String surname){
		super(name, surname);
		salary = 300;
	}
}
