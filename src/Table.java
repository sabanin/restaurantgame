/**
 * @(#) Table.java
 */

public class Table
{
	private Waiter waiter;
	
	private int number;
	
	public Table(int number){
		this.number = number;
	}
	
	public Waiter getWaiter() {
		return waiter;
	}

	public void setWaiter(Waiter waiter) {
		this.waiter = waiter;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
}
